package cviceni;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Ukol3Test {

	StringBuilder sb;
	StringBuffer sbuff;


	@BeforeClass
	public void init() {
		sb = new StringBuilder();
		sbuff = new StringBuffer();

	}

	@Test(invocationCount = 1)
	public void testStringBuilder() throws InterruptedException {
		StringBuilderTest[] tests = new StringBuilderTest[100];

		for (int i = 0; i < tests.length; i++) {
			tests[i] = new StringBuilderTest(sb);
		}

		for (int i = 0; i < tests.length; i++) {
			tests[i].start();
		}

		for (int i = 0; i < tests.length; i++) {
			tests[i].join();
		}

		Assert.assertEquals(sb.length(), 10000);

	}

	@Test( invocationCount = 1)
	public void testStringBuffer() throws InterruptedException {

		StringBufferTest[] tests = new StringBufferTest[100];

		for (int i = 0; i < tests.length; i++) {
			tests[i] = new StringBufferTest(sbuff);
		}

		for (int i = 0; i < tests.length; i++) {
			tests[i].start();
		}

		for (int i = 0; i < tests.length; i++) {
			tests[i].join();
		}

		Assert.assertEquals(sbuff.length(), 10000);

	}


}

// Every run would attempt to append 100 "A"s to the StringBuilder.
class StringBuilderTest extends Thread {

	StringBuilder sb;

	public StringBuilderTest(StringBuilder sb) {
		this.sb = sb;
	}

	@Override
	public void run() {
		for (int i = 0; i < 100; i++) {
			sb.append("A");
		}

	}
}

// Every run would attempt to append 100 "A"s to the StringBuffer.
class StringBufferTest extends Thread {

	StringBuffer sb2;

	public StringBufferTest(StringBuffer sb2) {
		this.sb2 = sb2;
	}

	@Override
	public void run() {
		for (int i = 0; i < 100; i++) {
			sb2.append("A");
		}

	}
}