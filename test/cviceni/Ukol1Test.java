package cviceni;

import java.math.BigDecimal;

import org.testng.Assert;
import org.testng.annotations.Test;

public class Ukol1Test {

	@Test
	public void testFaktorial1() {
		Assert.assertEquals(Ukol1.faktorial(0), 1);
		Assert.assertEquals(Ukol1.faktorial(1), 1);
	}

	@Test(groups="todo", expectedExceptions=IllegalArgumentException.class)
	public void testFaktorial2() {
		Ukol1.faktorial(-1);
	}

	@Test(groups = "todo")
	public void testFaktorial3() {
		int result=Ukol1.faktorial(18);
		//nevedel jsem jak osetrit preteceni intu, 
		//tak jsem spocital faktorial 18 a porovnavam ho jako bigdecimal
		Assert.assertEquals(new BigDecimal(result),new BigDecimal(6402373705728000l));
		
		
	}
}
