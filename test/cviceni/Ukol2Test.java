package cviceni;

import org.testng.Assert;
import org.testng.annotations.Test;

import org.testng.annotations.DataProvider;


public class Ukol2Test {

	@Test(dataProvider="kratData")
	public void testKrat(int a, int b, int expected) {
	
		Assert.assertEquals(Ukol2.krat(a, b), expected);
		
		
		
	
	}
	
	@DataProvider(name="kratData")
	public Object[][] kratProvider(){
		return new Object[][]{{5,2,10},{-5,4,-20},{4,-5,-20},{-4,-5,20},{0,2,0},{2,0,0},{0,0,0},{1000000000,1,1000000000}};
	}

}
